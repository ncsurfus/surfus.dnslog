﻿/*Created By: Nathan Surfus
 * An example of how to use dnsLog.
 * This application will recieve parsed packets from dnsLog and create a list of unique domains from successful replies.
 * The list of unique domains is logged in the console and to a text file.
*/
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Surfus.dnsLog.Parser;

namespace Surfus.dnsLog.App
{
    public class dnsLogApp
    {
        private string[] domainFilter { get; set; }
        List<Record> dnsRecords { get; set; }
        StreamWriter textLog { get; set; }
        SharpPcap.ICaptureDevice Device { get; set; }

        public dnsLogApp(string LogPath, string[] domainList)
        {
            domainFilter = domainList;
            dnsRecords = new List<Record>();
            textLog = new StreamWriter(File.Open(LogPath, FileMode.Append));
            textLog.AutoFlush = true;

            var CaptureDevice = PickDevice();
            if(CaptureDevice != null)
            {
                dnsListener Listener = new dnsListener(CaptureDevice);
                Listener.PacketException += Listener_PacketException;
                Listener.PacketParsed += Listener_PacketParsed;
                Listener.Start();
                if(Listener.isCapturing)
                {
                    quitListening();
                    Listener.Stop();
                }
            }
        }

        void Listener_PacketParsed(object Sender, dnsPacket Packet)
        {
            if (Packet != null && Packet.ReplyCode == ResponseCodes.NoError && Packet.AllRecords != null && Packet.AllRecords.Length > 0)
            {
                foreach (var dnsRecord in Packet.AllRecords)
                {
                    string dnsName = dnsRecord.Name;
                    for (int i = 0; i != domainFilter.Length; i++)
                    {
                        if (dnsName.EndsWith(domainFilter[i]))
                        {
                            Record Found = dnsRecords.Find(x => x.Name.ToString() == dnsName.ToString());
                            if (Found == null)
                            {
                                dnsRecords.Add(dnsRecord);
                                textLog.WriteLine(dnsName);
                                Console.WriteLine(dnsName);
                            }
                        }
                    }
                }
            }
        }

        void Listener_PacketException(object Sender, Exception Ex, byte[] Buffer, int Start)
        {
            StringBuilder exceptionText = new StringBuilder();
            exceptionText.AppendLine("### EXCEPTION START ###");
            exceptionText.AppendLine("Exception : " + Ex.ToString());
            exceptionText.AppendLine("Buffer : " + Ex.ToString());
            for(int i = Start; i != Buffer.Length; i++)
            {
                exceptionText.Append(Buffer[i].ToString() + " ");
            }
            exceptionText.AppendLine("### EXCEPTION END ###");

            textLog.WriteLine(exceptionText.ToString());
            Console.WriteLine(exceptionText.ToString());
        }

        private SharpPcap.ICaptureDevice PickDevice()
        {
            foreach(var captureDevice in dnsListener.captureDevices)
            {
                Console.WriteLine(captureDevice.Description);
                if (useDevice())
                {
                    return captureDevice;
                }
            }

            return null;
        }
        private bool useDevice()
        {
            Console.WriteLine("Use this device? y or n.");
            switch (Console.ReadLine().ToLower())
            {
                case "y":
                    return true;
                case "n":
                    return false;
                default:
                    return useDevice();
            }
        }
        private bool quitListening()
        {
            Console.WriteLine("Type \"quit\" to stop.");
            switch (Console.ReadLine().ToLower())
            {
                case "quit":
                    return true;
                default:
                    return quitListening();
            }
        }
    }
}
