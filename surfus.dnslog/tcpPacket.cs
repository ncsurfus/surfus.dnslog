﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Surfus.dnsLog
{
    public partial class DNS
    {
        public delegate void parseCallBack(byte[] buffer, int start);

        public class tcpConnection
        {
            private parseCallBack packetComplete;
            public string ipAddress;
            public UInt16 Port;

            private byte[] Buffer = new byte[UInt16.MaxValue];
            private UInt16 packetSize { get; set; }
            private UInt16 bufferIndex { get; set; }

            public tcpConnection(string ipaddress, UInt16 port, parseCallBack packetcomplete)
            {
                ipAddress = ipaddress;
                Port = port;
                packetComplete = packetcomplete;
            }
            
            public void AddData(byte[] Data)
            {
                if (Data != null && Data.Length > 0)
                {
                    int dataOffset = 0;

                    if (packetSize == 0 && bufferIndex == 1)
                    {
                        //Perviously received only 1 byte.
                        byte[] psBuffer = new byte[2] { Buffer[0], Data[0] };
                        packetSize = Convert.ToUInt16(IPAddress.NetworkToHostOrder(BitConverter.ToInt16(psBuffer, 0)));

                        //Clear packetSize information from buffer.
                        bufferIndex = 0;
                        Buffer[0] = 0;

                        //Set offset so we don't add packetSize information into buffer.
                        dataOffset = 1;
                    }
                    else if (packetSize == 0 && Data.Length == 1)
                    {
                        //Need at least 2 bytes to get packet length. Only got 1
                        Buffer[0] = Data[0];
                        bufferIndex++;
                        return;
                    }
                    else if (packetSize == 0 && Data.Length >= 2)
                    {
                        //Received enough bytes to get data.
                        packetSize = Convert.ToUInt16(IPAddress.NetworkToHostOrder(BitConverter.ToInt16(Data, 0)));

                        //Set offset so we don't add packetSize information into buffer.
                        dataOffset = 2;
                    }

                    //Add Data to buffer without length.
                    int remainingIndex = addBuffer(Data, dataOffset);

                    if (bufferIndex == packetSize && packetComplete != null)
                    {
                        packetComplete(Buffer, 0);

                        //Reset Variables for next possible incoming packet
                        Buffer = new byte[UInt16.MaxValue];
                        packetSize = 0;
                        bufferIndex = 0;

                        //What if we got 2 dns packets in the same TCP packet? Continue processing.
                        if(remainingIndex != -1)
                        {
                            byte[] newData = new byte[Data.Length - remainingIndex];
                            Buffer.CopyTo(newData, remainingIndex);
                        }
                    }
                }
            }

            private int addBuffer(byte[] Data, int start = 0)
            {
                for(int i = start; i < Data.Length; i++)
                {
                    if(bufferIndex >= Buffer.Length || bufferIndex >= packetSize)
                    {
                        return i;
                    }
                    else
                    {
                        Buffer[bufferIndex] = Data[i];
                        bufferIndex++;
                    }
                }
                return -1;
            }
        }
    }
}
