﻿/*Created By: Nathan Surfus
 * The dnsListener class uses SharpPcap and PacketDotNet to capture UDP and TCP DNS packets in promiscuous mode.
*/
using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using Surfus.dnsLog.Parser;
using SharpPcap;
using SharpPcap.WinPcap;
using PacketDotNet;

namespace Surfus.dnsLog
{
    public delegate void packetParsed(object Sender, dnsPacket Packet);
    public delegate void packetError(object Sender, Exception Ex, byte[] Buffer, int Start);

    public class dnsListener
    {
        /// <summary>Gets the capture devices.</summary>
        /// <value>The capture devices.</value>
        public static CaptureDeviceList captureDevices
        {
            get
            {
                CaptureDeviceList.Instance.Refresh();
                return CaptureDeviceList.Instance;
            }
        }

        /// <summary>Gets the event that is executed when a packet is parsed.</summary>
        /// <value>The event that is executed when a packet is parsed</value>
        public event packetParsed PacketParsed;

        /// <summary>Gets the event that is executed when a packet failed parsing.</summary>
        /// <value>The event that is executed when a packet failed parsing</value>
        public event packetError PacketException;

        /// <summary>Provides thread concurrency for the packetQueue.</summary>
        /// <value>The object to be lock providing thread concurrency for the packetQueue.</value>
        private readonly object packetQueueLock = new object();

        /// <summary>Gets the current queue of packets that need to be parsed.</summary>
        /// <value>The list of captured domains.</value>
        private List<RawCapture> packetQueue { get; set; }

        /// <summary>Gets the current queue of TCP Connections.</summary>
        /// <value>The list of TCP Connections.</value>
        private List<DNS.tcpConnection> tcpQueue { get; set; }

        /// <summary>Gets the device to capture packets on.</summary>
        /// <value>The device to capture packets on.</value>
        private ICaptureDevice captureDevice { get; set; }

        /// <summary>Gets the current state of capturing packets.</summary>
        /// <value>The current state of capturing packets (On or Off).</value>
        public bool isCapturing { get { return captureDevice != null ? captureDevice.Started : false; } }

        /// <summary>Gets the thread that processes packets from the packetQueue.</summary>
        /// <value>The thread that processes packets from the packetQueue.</value>
        private Thread packetThread { get; set; }

        /// <summary>Creates the dnsListener and sets the capture device.</summary>
        /// <param name="Device">The device to capture packets from. Use the static method captureDevices to retrieve a list.</param>
        public dnsListener(ICaptureDevice Device)
        {
            packetQueue = new List<RawCapture>();
            tcpQueue = new List<DNS.tcpConnection>();
            captureDevice = Device;
        }

        /// <summary>Starts the capturing of Packets.</summary>
        public void Start()
        {
            if(captureDevice != null && !captureDevice.Started)
            {
                //Start Device
                captureDevice.OnPacketArrival += OnPacketArrival;
                captureDevice.Open(DeviceMode.Promiscuous, 1000);
                captureDevice.Filter = "src port 53";
                captureDevice.StartCapture();

                //Create packetThread Thread
                packetThread = new Thread(() => { ProcessPacketQueue(); });
                packetThread.Start();
            }
            else
            {
                throw new Exception("No capture device to start or capture already started.");
            }
        }

        /// <summary>Stops the capturing of Packets.</summary>
        public void Stop()
        {
            if(captureDevice != null && captureDevice.Started)
            {
                captureDevice.StopCapture();
                captureDevice.Close();

                if(packetThread != null && packetThread.IsAlive)
                {
                    packetThread.Abort();
                    packetThread.Join();
                }
            }
            else
            {
                throw new Exception("No capture device to stop or capture was never started.");
            }
        }

        /// <summary>Executed when a new packet arrives. Adds the packet to the packetQueue.</summary>
        private void OnPacketArrival(object sender, CaptureEventArgs e)
        {
            lock (packetQueueLock)
            {
                packetQueue.Add(e.Packet);
            }
        }

        /// <summary>Executed on its own thread and processes packets in that packetQueue.</summary>
        private void ProcessPacketQueue()
        {
            while (captureDevice.Started)
            {
                List<RawCapture> ourQueue = null;
                lock (packetQueueLock)
                {
                    if (packetQueue.Count > 0)
                    {
                        ourQueue = packetQueue;
                        packetQueue = new List<RawCapture>();
                    }
                }

                if (ourQueue != null)
                {
                    for (int i = 0; i != ourQueue.Count; i++)
                    {
                        ProcessPacket(ourQueue[i]);
                        Thread.Sleep(5);
                    }
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }

        /// <summary>Gets the payload from a TCP or UDP packet.</summary>
        /// <param name="packet">The packet to be processed.</param>
        private void ProcessPacket(RawCapture packet)
        {
            Packet genericPacket = Packet.ParsePacket(packet.LinkLayerType, packet.Data);
            IPv4Packet ipPacket = new IPv4Packet(genericPacket.PayloadPacket.BytesHighPerformance);
            if (ipPacket.Protocol == IPProtocolType.UDP && ipPacket.ValidChecksum)
            {
                UdpPacket udpPacket = new UdpPacket(ipPacket.PayloadPacket.BytesHighPerformance);
                if (udpPacket.PayloadData != null && udpPacket.SourcePort == 53)
                {
                    ParsePacket(udpPacket.PayloadData);
                }
            }
            else if (ipPacket.Protocol == IPProtocolType.TCP)
            {
                TcpPacket tcpPacket = new TcpPacket(ipPacket.PayloadPacket.BytesHighPerformance);
                if (tcpPacket.PayloadData != null && tcpPacket.SourcePort == 53)
                {
                    ProcessTCP(tcpPacket, ipPacket);
                }
            }
        }

        /// <summary>Peforms additional steps to process a TCP packet.</summary>
        /// <param name="tcpPacket">The TCP packet to be processed.</param>
        /// <param name="ipPacket">The IP packet that encapsulates tcpPacket.</param>
        private void ProcessTCP(TcpPacket tcpPacket, IpPacket ipPacket)
        {
            //Get IP and Port from the packet.
            //We don't care about the source port, as we are filtering that.
            string ipAddress = ipPacket.DestinationAddress.ToString();
            UInt16 port = tcpPacket.DestinationPort;

            //Find the connection in tcpQueue.
            DNS.tcpConnection tcpConnection = tcpQueue.Find(x => x.ipAddress == ipAddress && x.Port == port);

            //Connection is forming.
            if (tcpPacket.Syn)
            {
                //If tcpConnection is null, we do not have this connection logged in our tcpQueue.
                if (tcpConnection == null)
                {
                    //Create and add this connection into our tcpQueue.
                    tcpQueue.Add(new DNS.tcpConnection(ipAddress, port, ParsePacket));
                }
            }
            //Connection has finished
            else if (tcpPacket.Fin && tcpConnection != null)
            {
                //If we have this connection on our tcpQueue remove it.
                tcpQueue.Remove(tcpConnection);
            }
            //We received data for the connection.
            //Smallest TCP packet is 20 bytes. If the TCP packet is under 20 bytes there will be padding, and it will show up under the payload. We need to ignore that.
            else if (tcpConnection != null && ipPacket.PayloadLength > 20)
            {
                //If we have the connection in tcpQueue add the data to it.
                tcpConnection.AddData(tcpPacket.PayloadData);
            }
        }

        /// <summary>Parses the DNS packet.</summary>
        /// <param name="Packet">The packet buffer to be parsed.</param>
        private void ParsePacket(byte[] Packet)
        {
            ParsePacket(Packet, 0);
        }

        /// <summary>Parses the DNS packet.</summary>
        /// <param name="Packet">The packet buffer to be parsed.</param>
        /// <param name="start">The index to begin parsing the DNS packet.</param>
        private void ParsePacket(byte[] Packet, int start)
        {
            if(Packet != null)
            {
                try
                {
                    if (PacketParsed != null)
                    {
                        //Attempt to parse packet
                        dnsPacket parsedPacket = new dnsPacket(Packet, start);
                        PacketParsed(this, parsedPacket);
                    }
                }
                catch (Exception Ex)
                {
                    if (PacketException != null)
                    {
                        PacketException(this, Ex, Packet, 0);
                    }
                }
            }
        }
    }
}
