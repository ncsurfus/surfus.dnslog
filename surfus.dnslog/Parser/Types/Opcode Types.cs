﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Surfus.dnsLog.Parser
{
    public enum OpCodes : int
    {
        Query = 0,
        IQuery = 1,
        Status = 2,
        Notify = 4,
        Update = 5
    }
}
