﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Surfus.dnsLog.Parser
{
    public enum ResponseCodes : int
    {
        NoError = 0,
        FormErr = 1,
        ServFail = 2,
        NXDomain = 3,
        NotImp = 4,
        Refused = 5,
        YXDomain = 6,
        YXRRSet = 7,
        NXRRSet = 8,
        NotAuth = 9,
        NotZone = 10,
        Bad_Vers_Or_Sig = 16,
        BadKey = 17,
        BadTime = 18,
        BadMode = 19,
        BadALG = 21,
        BadTrunc = 22
    }
}
