﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Surfus.dnsLog.Parser
{
    public enum ClassTypes : int
    {
        INTERNET = 1,
        CSNET = 2,
        CHAOS = 3,
        HESIOD = 4,
        NONE = 254,
        ALL = 255
    }
}
