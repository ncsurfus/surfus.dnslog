﻿using System;
using Surfus.dnsLog.Parser.Extensions;

namespace Surfus.dnsLog.Parser
{
    public class DnsFlags
    {
        public UInt16 RawFlags { get; set; }
        public bool isResponse { get; set; }
        public bool isAuthoritative { get; set; }
        public bool isTruncated { get; set; }
        public bool isRecursionDesired { get; set; }
        public bool isRecursionAvailable { get; set; }
        public bool isReservedBitSet { get; set; }
        public bool isAuthenticated { get; set; }
        public bool isNoAuthAcceptable { get; set; }
        public OpCodes Opcode { get; set; }
        public ResponseCodes ReplyCode { get; set; }

        public DnsFlags(UInt16 flags)
        {
            RawFlags = flags;
            ProcessFlags();
        }

        private void ProcessFlags()
        {
            isResponse = RawFlags.GetBit(15);
            Opcode = (OpCodes)((RawFlags & 30720) >> 11);
            isAuthoritative = RawFlags.GetBit(10);
            isTruncated = RawFlags.GetBit(9);
            isRecursionDesired = RawFlags.GetBit(8);
            isRecursionAvailable = RawFlags.GetBit(7);
            isReservedBitSet = RawFlags.GetBit(6);
            isAuthenticated = RawFlags.GetBit(5);
            isNoAuthAcceptable = RawFlags.GetBit(4);
            ReplyCode = (ResponseCodes)(RawFlags & 15);
        }
    }
}
