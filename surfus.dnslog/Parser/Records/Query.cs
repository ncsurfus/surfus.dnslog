﻿using System;
using Surfus.dnsLog.Parser.Extensions;

namespace Surfus.dnsLog.Parser
{
    public class Query
    {
        public string Name = "";
        public RecordTypes Type;
        public ClassTypes Class;

        public Query(string name, RecordTypes type, ClassTypes classType)
        {
            Name = name;
            Type = type;
            Class = classType;
        }
    }
}
