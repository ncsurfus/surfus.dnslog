﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Surfus.dnsLog.Parser.Extensions;

namespace Surfus.dnsLog.Parser
{
    public class Record
    {
        public string Name;
        public RecordTypes Type;
        public ClassTypes Class;
        public UInt32 TTL;
        public UInt16 DataLength;
        public RecordStyle Style;
        public byte[] RecordData;

        public Record(string name, RecordTypes type, ClassTypes classType, UInt32 ttl, UInt16 dataLength, byte[] recordData)
        {
            Name = name;
            Type = type;
            Class = classType;
            TTL = ttl;
            DataLength = dataLength;
            RecordData = recordData;
            Style = RecordStyle.Answer;
        }
    }
}
