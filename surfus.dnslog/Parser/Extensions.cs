﻿using System;
using System.Net;

namespace Surfus.dnsLog.Parser.Extensions
{
    public static class Binary
    {
        public static bool GetBit(this UInt16 b, int bitNumber)
        {
            return (b & (1 << bitNumber)) != 0;
        }
        public static UInt16 GetUInt16(this byte[] Buffer, int startIndex)
        {
            return (UInt16)IPAddress.NetworkToHostOrder((Int16)BitConverter.ToUInt16(Buffer, startIndex));
        }
        public static UInt32 GetUInt32(this byte[] Buffer, int startIndex)
        {
            return (UInt32)IPAddress.NetworkToHostOrder((Int32)BitConverter.ToUInt32(Buffer, startIndex));
        }
        public static UInt16 ToNetworkOrder(this UInt16 Var)
        {
            return (UInt16)IPAddress.HostToNetworkOrder(Var);
        }
        public static UInt32 ToNetworkOrder(this UInt32 Var)
        {
            return (UInt32)IPAddress.HostToNetworkOrder(Var);
        }
    }
}
