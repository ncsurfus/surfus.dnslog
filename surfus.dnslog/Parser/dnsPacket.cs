﻿/*Created By: Nathan Surfus
 * The dnsPacket class parses a DNS Packet into the fields defined by the DNS specifications.
 * The Constructor does all the work, nothing else needs to be called to parse the DNS packet.
 * TODO: Create a method to generate a DNS Packet.
*/
using System;
using System.Net;
using System.Linq;
using System.Collections.Generic;
using Surfus.dnsLog.Parser.Extensions;

namespace Surfus.dnsLog.Parser
{
    public class dnsPacket
    {
        /// <summary>Gets or sets the DNS packet.</summary>
        /// <value>The DNS packet buffer.</value>
        protected byte[] Buffer { get; set; }

        /// <summary>Gets the index offset of the DNS packet.</summary>
        /// <value>The index offset of the DNS packet buffer.</value>
        protected int Offset { get; set; }

        /// <summary>Gets the current index of the DNS packet.</summary>
        /// <value>The index of the DNS packet buffer.</value>
        protected int Position { get; set; }

        /// <summary>Gets the TransactionID of the DNS packet.</summary>
        /// <value>The TransactionID of the DNS packet.</value>
        public UInt16 TransactionID { get; set; }

        /// <summary>Gets the individual Flags of the DNS packet.</summary>
        /// <value>The individual Flags of the DNS packet.</value>
        public DnsFlags Flags { get; set; }

        /// <summary>Gets the amount of answers specified by the DNS packet.</summary>
        /// <value>The amount of answers specified by the DNS packet.</value>
        public UInt16 AnswerCount;

        /// <summary>Gets the amount of authoritative answers specified by the DNS packet.</summary>
        /// <value>The amount of authoritative answers specified by the DNS packet.</value>
        public UInt16 AuthoritativeCount;

        /// <summary>Gets the amount of additional answers specified by the DNS packet.</summary>
        /// <value>The amount of additional answers specified by the DNS packet.</value>
        public UInt16 AdditionalCount;

        /// <summary>Gets the reply code from the DNS packet's flags.</summary>
        /// <value>The reply code from the DNS packet's flags.</value>
        public ResponseCodes ReplyCode { get { return Flags.ReplyCode; } }

        /// <summary>Gets the queries from the DNS packet.</summary>
        /// <value>The queries from the DNS packet.</value>
        public Query[] Queries { get; set; }

        /// <summary>Gets the records from the DNS packet.</summary>
        /// <value>The records from the DNS packet.</value>
        public Record[] AllRecords { get; set; }

        /// <summary>Gets the answer records from the DNS packet.</summary>
        /// <value>The answer records from the DNS packet.</value>
        public IEnumerable<Record> AnswerRecords
        {
            get
            {
                return AllRecords.Where(R => R.Style == RecordStyle.Answer);
            }
        }

        /// <summary>Gets the authoritative records from the DNS packet.</summary>
        /// <value>The authoritative records from the DNS packet.</value>
        public IEnumerable<Record> AuthoritativeRecords
        {
            get
            {
                return AllRecords.Where(R => R.Style == RecordStyle.Authoritative);
            }
        }

        /// <summary>Gets the additional records from the DNS packet.</summary>
        /// <value>The additional records from the DNS packet.</value>
        public IEnumerable<Record> AdditonalRecords
        {
            get
            {
                return AllRecords.Where(R => R.Style == RecordStyle.Additonal);
            }
        }

        /// <summary>Returns true if there is additional data at the end of the DNS packet.</summary>
        /// <value>The value of additional data at the end of the DNS packet.</value>
        public bool isExtraData
        {
            get
            {
                if(Buffer != null)
                {
                    if(Position < Buffer.Length)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>Gets the additional data at the end of the DNS packet.</summary>
        /// <value>The additional data at the end of the DNS packet.</value>
        public IEnumerable<byte> getExtraData
        {
            get
            {
                if(Buffer != null)
                {
                    if(Position < Buffer.Length)
                    {
                        return Buffer.Skip(Position);
                    }
                }
                throw new Exception("No extra data to get");
            }
        }

        /// <summary>Starts the process of parsing a DNS packet.</summary>
        /// <param name="buffer">The array of bytes that make the DNS packet.</param>
        /// <param name="startIndex">The index to begin reading the DNS packet.</param>
        public dnsPacket(byte[] buffer, int startIndex = 0)
        {
            //Set Initial Variables.
            Buffer = buffer;
            Position = startIndex;
            Offset = startIndex;

            //Parse DNS Packet Header.
            TransactionID = getUInt16();
            Flags = new DnsFlags(getUInt16());

            //Parse DNS Packet Queries.
            Queries = new Query[getUInt16()];

            //Parse DNS Packet Record Counts.
            AnswerCount = getUInt16();
            AuthoritativeCount = getUInt16();
            AdditionalCount = getUInt16();

            //Create array to hold DNS Packet Records.
            AllRecords = new Record[AnswerCount + AuthoritativeCount + AdditionalCount];

            //Parse DNS Packet Queries and Records
            readQueries();
            readAnswers();
            readAuthoritative();
            readAdditional();
        }

        /// <summary>Creates an empty DNS packet.</summary>
        public dnsPacket(){}

        /// <summary>Reads the DNS packet queries and places the result in Queries.</summary>
        protected void readQueries()
        {
            for (int i = 0; i < Queries.Length; i++)
            {
                string Name = getName();
                RecordTypes Type = (RecordTypes)getUInt16();
                ClassTypes Class = (ClassTypes)getUInt16();
                Queries[i] = new Query(Name, Type, Class);
            }
        }

        /// <summary>Reads the DNS packet answer records and places the result in AllRecords.</summary>
        protected void readAnswers()
        {
            for (int i = 0; i < AnswerCount; i++)
            {
                AllRecords[i] = ProcessRecord();
                AllRecords[i].Style = RecordStyle.Answer;
            }
        }

        /// <summary>Reads the DNS packet authoritative records and places the result in AllRecords.</summary>
        protected void readAuthoritative()
        {
            for (int i = 0; i < AuthoritativeCount; i++)
            {
                AllRecords[i + AnswerCount] = ProcessRecord();
                AllRecords[i + AnswerCount].Style = RecordStyle.Authoritative;
            }
        }

        /// <summary>Reads the DNS packet additional records and places the result in AllRecords.</summary>
        protected void readAdditional()
        {
            for (int i = 0; i < AdditionalCount; i++)
            {
                AllRecords[i + AnswerCount + AuthoritativeCount] = ProcessRecord();
                AllRecords[i + AnswerCount + AuthoritativeCount].Style = RecordStyle.Authoritative;
            }
        }

        /// <summary>Reads the DNS packet authoritative records and places the result in AllRecords.</summary>
        protected Record ProcessRecord()
        {
            //string Name = readLabel(Position);
            string Name = getName();
            RecordTypes Type = (RecordTypes)getUInt16();
            ClassTypes Class = (ClassTypes)getUInt16();
            UInt32 TTL = getUInt32();
            UInt16 DataLength = getUInt16();
            byte[] RecordData = getData(DataLength);
            return new Record(Name, Type, Class, TTL, DataLength, RecordData);
        }

        /// <summary>Returns a unsigned 16 bit integer from Buffer at the index of Position. Increases Position by 2.</summary>
        protected UInt16 getUInt16()
        {
            if(Position + 2 <= Buffer.Length)
            {
                UInt16 Value = (UInt16)IPAddress.NetworkToHostOrder((Int16)BitConverter.ToUInt16(Buffer, Position));
                Position += 2; ;
                return Value;
            }
            else
            {
                throw new Exception("UInt16 exceeds packet size.");
            }
        }

        /// <summary>Returns a unsigned 32 bit integer from Buffer at the index of Position. Increases Position by 4.</summary>
        protected UInt32 getUInt32()
        {
            if (Position + 4 <= Buffer.Length)
            {
                UInt32 Value = (UInt32)IPAddress.NetworkToHostOrder((Int32)BitConverter.ToUInt32(Buffer, Position));
                Position += 4;
                return Value;
            }
            else
            {
                throw new Exception("UInt32 exceeds packet size.");
            }
        }

        /// <summary>Returns an array of bytes from Buffer at the index of Position. Increases Position by Length.</summary>
        /// <param name="Length">The amount of bytes to read from Buffer.</param>
        protected byte[] getData(UInt16 Length)
        {
            if (Position + Length <= Buffer.Length)
            {
                byte[] Data = new byte[Length];
                Array.Copy(Buffer, Position, Data, 0, Length);
                Position += Length;
                return Data;
            }
            else
            {
                throw new Exception("RData exceeds packet size.");
            }
        }

        /// <summary>Returns a name from the current Query or Record.</summary>
        protected string getName()
        {
            string Name = "";

            while (Position < Buffer.Length)
            {
                int labelType = Buffer[Position];
                Position++;

                //Compressed Label
                if (labelType >= 192)
                {
                    //Compressed labels terminate Names.
                    //ANDing with 16383 will force the first 2 bits to equal 0. Our pointer is the last 14 bits.
                    Position--;//Backup Position, so we can read the full 16bits of the pointer.
                    Name += readCompressedLabel((UInt16)(getUInt16() & 16383) + Offset);
                    return Name;
                }
                else if (labelType == 0)
                {
                    //Root Label
                    return Name;
                }
                else if (labelType <= 63)
                {
                    int labelSize = labelType;
                    Name += readStandardLabel(Position, labelSize);
                    Position += labelSize;
                }
                else
                {
                    throw new Exception("Unknown or deprecated label type.");
                }
            }
            throw new Exception("No root label.");
        }

        /// <summary>Returns a standard label at the specified index of Buffer.</summary>
        /// <param name="index">The index of Buffer to begin reading the label.</param>
        /// <param name="labelLength">Length of the label to read.</param>
        protected string readStandardLabel(int index, int labelLength)
        {
            string Label;
            if (index + labelLength < Buffer.Length)
            {
                Label = System.Text.ASCIIEncoding.ASCII.GetString(Buffer, index, labelLength) + '.';
                return Label;
            }
            else
            {
                throw new Exception("Standard label is too big for buffer.");
            }
        }

        /// <summary>Returns an extended label starting at the index of Buffer.</summary>
        /// <param name="Pointer">The index of of the pointer of the extended label.</param>
        protected string readCompressedLabel(int Pointer)
        {
            string Name = "";
            while (Pointer < Buffer.Length)
            {
                int labelSize = Buffer[Pointer];
                Pointer++;

                if (labelSize == 0 || labelSize >= 192)
                {
                    //Root Label
                    return Name;
                }
                else if (labelSize <= 63)
                {
                    Name += readStandardLabel(Pointer, labelSize);
                    Pointer += labelSize;
                }
                else
                {
                    throw new Exception("Bad compressed label.");
                }
            }
            throw new Exception("No root label at compressed label.");
        }
    }
}
